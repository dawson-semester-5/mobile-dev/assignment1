package com.example.bmicalculator

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

/**
 *  Calculates Body Mass Index (BMI) using user inputs of weight and height
 *
 * @author Neil Fisher (1939668)
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Add event listener to calculate button
        findViewById<Button>(R.id.calculate).setOnClickListener { v: View -> calcBMI(v) }
    }

    /**
     * @param btn The button that triggers this method
     */
    private fun calcBMI(btn: View) {
        // Close keyboard when button is pressed to show Health Risk info fully
        // Inspiration from -> https://stackoverflow.com/a/10442532/15071892
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(btn.windowToken, 0)

        //

        // Outputs
        val output = findViewById<TextView>(R.id.output)
        val healthRisk = findViewById<TextView>(R.id.health_risk)

        // Get weight
        val weightInput = findViewById<EditText>(R.id.weight)
        var weight = 0.0f
        try {
            weight = weightInput.text.toString().toFloat()
        } catch (e: NumberFormatException) {
            output.text = getString(R.string.invalid_weight)
        }

        // weight must be in kg
        val metricWeight = findViewById<Switch>(R.id.lbsSwitch).isChecked
        if (!metricWeight)
            weight /= 2.205f

        if (weight > 300.0f || weight < 10.0f) {
            output.text = getString(R.string.invalid_weight)
            healthRisk.text = getString(R.string.not_applicable)
            return
        }


        // Get height
        val heightInput = findViewById<EditText>(R.id.height)
        var height = 0.0f
        try {
            height = heightInput.text.toString().toFloat()
        } catch (e: NumberFormatException) {
            output.text = getString(R.string.invalid_height)
        }

        // height must be in meters(m)
        val metricHeight = findViewById<Switch>(R.id.metersSwitch).isChecked
        if (!metricHeight)
            height /= 3.281f

        if (height > 2.2f || height < 0.2f) {
            output.text = getString(R.string.invalid_height)
            healthRisk.text = getString(R.string.not_applicable)
            return
        }

        // calculate BMI
        val bodyMassIdx = (weight) / (height * height)
        val bodyMassIdxStr = bodyMassIdx.toString()
        output.text = bodyMassIdxStr.substring(0, bodyMassIdxStr.indexOf(".") + 3)

        // show output message depending on BMI
        if (bodyMassIdx < 18.5f) {
            healthRisk.text = getString(R.string.underweight)
        } else if (bodyMassIdx >= 18.5f && bodyMassIdx < 23.0f) {
            healthRisk.text = getString(R.string.normal)
        } else if (bodyMassIdx >= 23.0f && bodyMassIdx < 27.5f) {
            healthRisk.text = getString(R.string.mild_to_moderate_overweight)
        } else {
            healthRisk.text = getString(R.string.very_overweight_to_obese)
        }
    }
}